/* 
 * File:   part0.c
 * Author: Grant Gravallese (ggravall@ucsc.edu)
 *
 * Created on July 29, 2022, 1:45 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include "BOARD.h"

/*
 * 
 */
int main(void) {
    BOARD_Init();
    printf("Hello World\n");

    BOARD_End();

    while (1);
}

