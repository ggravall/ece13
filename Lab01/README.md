ggravall Grant Gravallese

[39, 33, 0, 2, 116]
[33, 39, 0, 2, 116]
[0, 33, 39, 2, 116]
[0, 2, 33, 39, 116]
[0, 2, 33, 39, 116]

The places where changes were made were lines 14, 26, 33, and 38. (as well as the comments I added).

In this lab, I had to set up git, as well as the IDE I will be using in this class. I stepped through a function, wrote a conversion table, and got Hello, World! to print on an OLED.
In this lab, I did not make any mistakes other than the ones the lab doc told me to. It was resolved through a TA though so all good. 
I would say that this lab is good but requires students to jump through a lot of hoops to get familiar with the software.