// **** Include libraries here ****
// Standard libraries
#include <stdio.h>

//Class specific libraries
#include "BOARD.h"


// User libraries
#include <stdio.h>
#include <stdlib.h>

int main(void) {
    BOARD_Init();
    /***************************************************************************************************
     * Your code goes in between this comment and the following one with asterisks.
     **************************************************************************************************/
    // Declare Variables
    float fahr, celsius;
    int lower, upper, step;

    // Initialize Variables
    lower = 0; // lower limit of temperature
    upper = 300; // upper limit
    step = 20; // step size
    fahr = lower;
    float kelvin = lower;

    char theF = 'F';
    char theC = 'C';
    printf("%05c %05c\n", theF, theC);
    // Print out table
    while (fahr <= upper) {
        celsius = (5.0 / 9.0)*(fahr - 32.0);
        printf("%7.1f %04.f\n", (double) fahr, (double) celsius);
        fahr = fahr + step;
    }

    printf("\n");

    char theK = 'K';

    printf("%06c %05c\n", theK, theF);

    while (kelvin <= upper) {
        fahr = ((kelvin - 273.15)*1.8) + 32.0;
        printf("%-03.3f %5f\n", (double) kelvin, (double) fahr);
        kelvin = kelvin + step;
    }

    //printf("\n");
    /***************************************************************************************************
     * Your code goes in between this comment and the preceding one with asterisks.
     **************************************************************************************************/

    // Returning from main() is bad form in embedded environments. So we sit and spin.
    while (1);
}
